---
- name: Reset `_vars_files` to allow multiple use of this role
  stat:
    path: file-of-no-consequence
  register: _vars_files
  changed_when: false

- name: Reset `_tasks_files` to allow multiple use of this role
  stat:
    path: file-of-no-consequence
  register: _tasks_files
  changed_when: false

- name: Set lowercase version of Ansible predefined OS variables
  set_fact:
    ansible_distribution_lowercase: "{{ ansible_distribution | lower | replace('\"', '') }}"
    ansible_distribution_version_lowercase: "{{ ansible_distribution_version | lower | replace('/', '_') }}"
    ansible_os_family_lowercase: "{{ ansible_os_family | lower }}"

# Due to how Debian manage it's minor release and how it presents them inside a Docker container, we only check the major.
- name: Set Debian version to not include minor version, only major
  when: ansible_distribution_lowercase == 'debian'
  set_fact:
    ansible_distribution_version_lowercase: "{{ ansible_distribution_version.split('.')[0] }}"

# For macOS, I let go of the patch version.
- name: Set MacOSX version to only major and minor
  when: ansible_distribution_lowercase == 'macosx'
  set_fact:
    ansible_distribution_version_lowercase: "{{ ansible_distribution_version.split('.')[0] }}.{{ ansible_distribution_version.split('.')[1] }}"

- name: Set an ordered list of OS files to check for first availability.
  set_fact:
    os_files:
      - "{{ ansible_distribution_lowercase }}-{{ ansible_distribution_version_lowercase }}.yml"
      - "{{ ansible_distribution_lowercase }}.yml"
      - "{{ ansible_os_family_lowercase }}.yml"

- name: Debug output of vars
  debug:
    msg:
      - "ansible_distribution                   == {{ ansible_distribution }}"
      - "ansible_distribution_lowercase         == {{ ansible_distribution_lowercase }}"
      - "ansible_distribution_version           == {{ ansible_distribution_version }}"
      - "ansible_distribution_version_lowercase == {{ ansible_distribution_version_lowercase }}"
      - "ansible_os_family                      == {{ ansible_os_family }}"
      - "ansible_os_family_lowercase            == {{ ansible_os_family_lowercase }}"
      - "os_files == {{ os_files }}"
    verbosity: 1

- name: Specific OS - Vars
  block:
    - name: Check existence of files in list of OS vars files
      delegate_to: localhost
      stat:
        path: "{{ utility_specific_os_role_vars_path }}/{{ file_name }}"
      loop: "{{ os_files }}"
      loop_control:
        loop_var: file_name
      register: _vars_files
      when: (_vars_files is not defined) or (_vars_files.stat.exists == false)
      ignore_errors: true
      changed_when: false

    - name: Set the first existing OS vars file
      delegate_to: localhost
      set_fact:
        first_vars_file: "{{ file.file_name }}"
      when: ('stat' in file) and (file.stat.exists == true)
      loop: "{{ _vars_files.results }}"
      loop_control:
        loop_var: file
        label: "{{ file.file_name }}"
      changed_when: false

    - name: Debug output `include_vars`
      debug:
        msg:
          - "utility_specific_os_role_vars_path == {{ utility_specific_os_role_vars_path }}"
          - "first_vars_file == {{ first_vars_file | default('') }}"
        verbosity: 1

    - name: Include vars
      when: (first_vars_file is defined) and (first_vars_file != '')
      include_vars:
        file: "{{ utility_specific_os_role_vars_path }}/{{ first_vars_file }}"

- name: Specific OS - Tasks
  block:
    - name: Check existence of files in list of OS tasks files
      delegate_to: localhost
      stat:
        path: "{{ utility_specific_os_role_tasks_path }}/{{ file_name }}"
      loop: "{{ os_files }}"
      loop_control:
        loop_var: file_name
      register: _tasks_files
      when: (_tasks_files is not defined) or (_tasks_files.stat.exists == false)
      ignore_errors: true
      changed_when: false

    - name: Set the first existing OS tasks file
      delegate_to: localhost
      set_fact:
        first_tasks_file: "{{ file.file_name }}"
      when: ('stat' in file) and (file.stat.exists == true)
      loop: "{{ _tasks_files.results }}"
      loop_control:
        loop_var: file
        label: "{{ file.file_name }}"
      changed_when: false

    - name: Debug output `first_tasks_file`
      debug:
        msg:
          - "utility_specific_os_role_tasks_path == {{ utility_specific_os_role_tasks_path }}"
          - "first_tasks_file == {{ first_tasks_file | default('') }}"
        verbosity: 1

    - name: Include tasks
      when: (first_tasks_file is defined) and (first_tasks_file != '')
      include_tasks:
        file: "{{ utility_specific_os_role_tasks_path }}/{{ first_tasks_file }}"
