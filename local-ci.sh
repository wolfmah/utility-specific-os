#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail
set -o xtrace

CONTAINER_IMAGE="debian:9" molecule test --scenario-name docker_empty_vars --no-all --destroy always
CONTAINER_IMAGE="debian:9" molecule test --scenario-name docker_empty_tasks --no-all --destroy always
CONTAINER_IMAGE="debian:9" molecule test --scenario-name docker_empty_vars_tasks --no-all --destroy always
CONTAINER_IMAGE="debian:9" molecule test --scenario-name docker_family_distribution_release --no-all --destroy always
CONTAINER_IMAGE="debian:9" molecule test --scenario-name docker_distribution_release --no-all --destroy always
CONTAINER_IMAGE="debian:9" molecule test --scenario-name docker_release --no-all --destroy always
CONTAINER_IMAGE="ubuntu:18.04" molecule test --scenario-name docker_family_distribution_release --no-all --destroy always
CONTAINER_IMAGE="ubuntu:18.04" molecule test --scenario-name docker_distribution_release --no-all --destroy always
CONTAINER_IMAGE="ubuntu:18.04" molecule test --scenario-name docker_release --no-all --destroy always
molecule test --scenario-name vagrant_openbsd_family_distribution_release --no-all --destroy always
molecule test --scenario-name vagrant_openbsd_distribution_release --no-all --destroy always
molecule test --scenario-name vagrant_openbsd_release --no-all --destroy always
